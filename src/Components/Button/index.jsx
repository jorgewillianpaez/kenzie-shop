import { ButtonContainer } from "./styles"

export default function Button({ children, ...rest }) {
    return (
        <ButtonContainer type="button" {...rest}>{children}</ButtonContainer>
    )
}