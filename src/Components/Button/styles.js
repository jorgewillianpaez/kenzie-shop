import styled from 'styled-components';

export const ButtonContainer = styled.button`

    padding: 8px;
    background-color: blueviolet;
    text-transform: uppercase;
    border: none;
    border-radius: 4px;
    :hover {
        background-color: violet;
    }

`;