import styled from 'styled-components';

export const Container = styled.div`

    div {
        span {
            color: var(--red);
        }
    }

`;

export const InputContainer = styled.div`

    input {
        padding: 6px;
        margin: 6px;
        border-radius: 4px;
        font-size: 16px;
        border: 1px solid transparent;
        :hover {
            border: 1px solid maroon;
        }
    }
    
`;