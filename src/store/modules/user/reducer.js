const defaultState = {
    token: ""
}

const userReducer = (state = defaultState, action) => {
    switch (action.type) {

        default:
            return state;
    };
};

export default userReducer;