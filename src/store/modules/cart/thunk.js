import { addToCart, removeFromCart } from "./actions";
import { toast } from "react-toastify";

export const addToCartThunk = (product) => {
    return (dispatch) => {
        const cartList = JSON.parse(localStorage.getItem("cart")) || [];
        const newCartList = [...cartList, product];

        toast.success("Produto adicionado ao carrinho!");
        localStorage.setItem("cart", JSON.stringify(newCartList));
        dispatch(addToCart(product));
    }
};

export const removeFromCartThunk = (id) => (dispatch, getStore) => {
    const { cart } = getStore();
    const newCartList = cart.filter((product) => product.id !== id);

    localStorage.setItem("cart", JSON.stringify(newCartList));
    dispatch(removeFromCart(newCartList));
};