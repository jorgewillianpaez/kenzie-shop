import styled from 'styled-components';

export const Container = styled.div`
    height: 100%;
    width: 100%;

    h1 {
        margin-left: 10px;
        margin-top: 5px;
    }
    
    h4 {
        position: absolute;
        left: 89%;
        top: 1%;
    }

    @media(min-width: 768px) {
        h4 {
            left: 96%;
        }
    }

`;

export const Cart = styled.div`
    display: flex;
    position: absolute;
    left: 60%;
    top: 2%;

    button {
        width: 100px;
        height: 30px;
    }

    @media(min-width: 768px) {
        left: 90%;
    }
    
`;

export const ProductsList = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    @media(min-width: 768px) {
        flex-direction: row;
        flex-wrap: wrap;
    }
    
`;

export const Product = styled.div`

    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    background-color: white;
    border: 1px solid cornflowerblue;
    border-radius: 2px;
    padding: 8px;
    width: 70%;
    margin: 10px;

    p {
        align-self: flex-start;
    }

    img {
        width: 90%;
    }

    @media(min-width: 768px) {
        
        margin: 10px;
        width: 30%;
        height: 40%;

        img {
            align-self: center;
            width: 180px;
        }

        p {
            margin: 2px;
        }
    }
    
`;