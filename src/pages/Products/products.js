const products = [
    { id: 1, name: "Headset HyperX Cloud Stinger", price: 350, img: "https://cdn.awsli.com.br/800x800/1146/1146018/produto/44285985/641bd26a24.jpg" },
    { id: 2, name: "Teclado HyperX Alloy Origins Core", price: 500, img: "https://media.kingston.com/hyperx/product/hx-product-keyboard-alloy-origins-core-no-1-zm-lg.jpg" },
    { id: 3, name: "Headset Razer Kraken", price: 400, img: "https://images6.kabum.com.br/produtos/fotos/109816/headset-razer-kraken-x-usb-rz04-02960100-r3u1_1581360634_g.jpg" },
    { id: 4, name: "Mouse HyperX pulsefire core", price: 200, img: "https://images6.kabum.com.br/produtos/fotos/98696/mouse-gamer-hyperx-pulsefire-core-hx-mc004b_mouse-gamer-hyperx-pulsefire-core-hx-mc004b_1539172781_g.jpg" },
    { id: 5, name: "Mousepad HyperX S-Fury", price: 100, img: "https://img.terabyteshop.com.br/produto/g/mouse-pad-gamer-hyperx-fury-s-control-grande-hx-mpfs-l_101323.jpg" },
    { id: 6, name: "Monitor Asus 240hz", price: 3000, img: "https://images0.kabum.com.br/produtos/fotos/96540/96540_1525287932_index_g.jpg" },
    { id: 7, name: "Placa de vídeo RTX 3060", price: 6000, img: "https://m.media-amazon.com/images/I/81k6bxrB2YL._AC_SL1500_.jpg" },
    { id: 8, name: "Mouse Razer", price: 300, img: "https://a-static.mlcdn.com.br/618x463/mouse-gamer-razer-deathadder-essential-6400-dpi-com-fio/shopb/shin-1403/aa883643e0ab13abc673ad9d6fc5a822.jpg" },
    { id: 9, name: "Teclado Razer", price: 500, img: "https://a-static.mlcdn.com.br/618x463/teclado-gamer-razer-blackwidow-v3-quartz-chroma-switch-green/megatumii/11432823927/775907317d048938bd1a173dda649734.jpg" }
]

export default products;