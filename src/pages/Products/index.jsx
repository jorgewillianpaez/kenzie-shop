import products from "./products";
import { Container, ProductsList, Product, Cart } from './styles';
import formatValue from "../../utils/formatValue";
import { useDispatch, useSelector } from "react-redux";
import { addToCartThunk } from "../../store/modules/cart/thunk";
import { useHistory } from "react-router-dom";
import Button from '../../Components/Button';

export default function Products() {

    const history = useHistory();
    const dispatch = useDispatch();
    const cartProducts = useSelector(store => store.cart);

    const handleClick = (product) => {
        return dispatch(addToCartThunk(product));
    }

    return (
        <Container>
            <h1>HardShop</h1>
            <Cart>
                <Button onClick={() => history.push('/cart')}>Carrinho</Button>
            </Cart>
            <h4>{cartProducts.length}</h4>
            <ProductsList>
                {products.map((product, index) => <Product key={index}>
                    <img src={product.img} alt={product.name} />
                    <p>{product.name}</p>
                    <p>{formatValue(product.price)}</p>
                    <Button onClick={() => handleClick(product)}>Adicionar ao carrinho</Button>
                </Product>)}
            </ProductsList>
        </Container>
    );
};