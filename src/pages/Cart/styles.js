import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
    flex-wrap: wrap;

    h3 {
        text-align: center;
        margin: 8px;
    }

    button {
        width: 200px;
        margin-left: 10px;
        margin-top: 10px;
    }
`;

export const Products = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;

    button {
        margin-top: 8px;
    }
`;

export const CartProduct = styled.div`
    
    background-color: thistle;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 240px;
    margin: 6px;
    padding: 16px;

    img {
        width: 120px;
    }

    @media(min-width: 768px) {

        width: 340px;

        img {
            width: 180px;
            align-self: center;
        }
    }
`;