import { useDispatch, useSelector } from "react-redux";
import { Container, CartProduct, Products } from './styles';
import formatValue from "../../utils/formatValue";
import Button from '../../Components/Button';
import { removeFromCartThunk } from "../../store/modules/cart/thunk";
import { useHistory } from "react-router-dom";

export default function Cart() {

    const dispatch = useDispatch();
    const cart = useSelector(store => store.cart);
    const history = useHistory();

    const totalPrice = cart.reduce((previousProduct, currentProduct) => currentProduct.price + previousProduct, 0);

    return (
        <Container>
            <Button onClick={() => history.push('/')}>Produtos</Button>
            <h3>Preço total: {formatValue(totalPrice)}</h3>
            <h3>Quantidade de itens no carrinho: {cart.length} produtos</h3>
            <Products>
                {cart.map((product, index) => <CartProduct key={index}>
                    {console.log(cart)}
                    <img src={product.img} alt={product.name} />
                    <p>Nome: {product.name}</p>
                    <p>Preço: {formatValue(product.price)}</p>
                    <Button onClick={() => dispatch(removeFromCartThunk(product.id))}>Remover do carrinho</Button>
                </CartProduct>)}
            </Products>
        </Container>
    );
};