import { Switch, Route } from 'react-router-dom';
import Cart from '../pages/Cart';
import Products from '../pages/Products';

export default function Routes() {
    return (
        <Switch>
            <Route exact path="/" component={Products} />
            {/* <Route path="/signup" component={Signup} />
            <Route path="/login" component={Login} /> */}
            <Route path="/cart" component={Cart} />
        </Switch>
    )
}