import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`

* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    outline: 0;
    text-decoration: none;
}

:root {
    --lato: 'Lato', sans-serif;
    --red: red;
}

body {
    font-family: var(--lato);
    font-size: 16px;
    background-image: linear-gradient(to right, #F78D45, #A3A1A8);
}

h1, h2, h3, h4, h5, h6 {
    font-weight: 800;
}

button {
    cursor: pointer;
}

`;